import com.google.gson.Gson;
import domain.Vehicle;
import hsl.Update;
import org.eclipse.paho.client.mqttv3.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.StringJoiner;
import java.util.concurrent.ConcurrentHashMap;

public class Subscriber implements MqttCallback {

    private IMqttClient client;
    private Gson gson;

    private String id;

    int counter = 0;

    private Map<String, Integer> vehicles = new ConcurrentHashMap<>();

    // TODO Use a custom Mqtt File Persistence to not
    // create a directory for each run...

    public Subscriber(String uri, String[] feed) {
        try {
            this.client = new MqttClient(uri,
                MqttAsyncClient.generateClientId());
            this.client.setCallback(this);
            this.gson = new Gson();

            id = sendPost("http://localhost:4567/a");

            // Why doesn't this continue?

            this.client.connect();
            this.client.subscribe(feed);

            System.out.println("Subscribed...");
        }

        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void connectionLost(Throwable throwable) {
        System.out.println("Connection lost!");
        throwable.printStackTrace();
        System.exit(1);
    }

    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        Update update = gson.fromJson(mqttMessage.toString(), Update.class);
        Update.VP values = update.vp;

        try {
            Vehicle veh = new Vehicle(values.operator, values.vehicle,
                values.latitude, values.longitude);
            System.out.println(veh);

            if (!vehicles.containsKey(veh.getIdentifier())) {
                vehicles.put(veh.getIdentifier(), 0);
            }

            counter++;

            if (counter >= 10) {
                fetchData();
                counter = 0;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
    }

    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
        System.out.println("What the heck is this?");
    }

    public void fetchData() {
        System.out.println("Fetching data...");
        System.out.println(sendPost("http://localhost:4567/o"));
    }

    public String sendPost(String address) {

        String ret;

        try {
            URL url = new URL(address);
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setRequestMethod("POST");
            con.setDoOutput(true);

            StringJoiner joiner = new StringJoiner("&");
            for (Map.Entry<String, Integer> entry : vehicles.entrySet()) {
                String key = URLEncoder.encode(entry.getKey(), "UTF-8");
                joiner.add(key + "=" + "0");
            }

            byte[] out = joiner.toString().getBytes(StandardCharsets.UTF_8);
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            con.setFixedLengthStreamingMode(out.length);
            con.connect();

            try (OutputStream os = con.getOutputStream()) {
                os.write(out);
            }

            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()))) {

                StringBuilder response = new StringBuilder();
                String line;

                while ((line = in.readLine()) != null) {
                    response.append(line);
                }

                ret = response.toString();
            }

            con.disconnect();
            return ret;

        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        return null;
    }

}
