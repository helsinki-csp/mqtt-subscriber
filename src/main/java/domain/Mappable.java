package domain;

public interface Mappable {

    String   getIdentifier();
    String[] getLocation();

}