package domain;

// domain.Entity is the most basic HSL entity from their APIs.
public abstract class Entity implements Mappable {

    private String[] location;

    public Entity(String lat, String lon) {
        this.location = new String[]{ lat, lon };
    }

    public Entity(String[] location) {
        this.location = location;
    }

    public String[] getLocation() {
        return this.location;
    }

}
