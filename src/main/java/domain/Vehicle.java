package domain;

public class Vehicle extends Entity {

    public String id;

    public Vehicle(String operator, String id,
        String lat, String lon) {
        super(lat, lon);
        this.id = constructId(operator, id);
    }

    private String constructId(String operator, String id) {
        return String.format("%s:%s", operator, id);
    }

    @Override
    public String toString() {
        String[] coord = getLocation();
        return String.format("Vehicle %s is at [%s, %s]",
            id, coord[0], coord[1]);
    }

    public String getIdentifier() {
        return id;
    }
}
