package domain;

public class Stop extends Entity {

    public Stop(String lat, String lon) {
        super(lat, lon);
    }

    public Stop(String[] location) {
        super(location);
    }

    public String getIdentifier() {
        return null;
    }

}
