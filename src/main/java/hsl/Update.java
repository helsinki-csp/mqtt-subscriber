package hsl;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Update {

    @SerializedName("VP")
    public VP vp;

    @Getter @Setter
    public class VP {

        @SerializedName ("oper") public String  operator;
        @SerializedName  ("veh") public String   vehicle;
        @SerializedName ("long") public String longitude;
        @SerializedName  ("lat") public String  latitude;

    }

    @Override
    public String toString() {
        return String.format("[OP & VE: %s %s]: %s %s", vp.operator,
            vp.vehicle, vp.latitude, vp.longitude);
    }

}
