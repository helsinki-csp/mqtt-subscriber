public class Application {

    private static Subscriber sub;

    private final static String HSL_API = "ssl://mqtt.hsl.fi:8883";

    private final static String[] area = { "/hfp/v2/journey/ongoing/vp/+/+/+/+/+/+/+/+/+/60;24/19/85/#",
            "/hfp/v2/journey/ongoing/vp/+/+/+/+/+/+/+/+/+/60;24/19/86/#" };

    private final static String[] test = { "/hfp/v2/journey/ongoing/vp/#" };

    public static void main(String... arg) {
        System.out.println("Running the demo MQTT application...");
        sub = new Subscriber(HSL_API, test);
    }

}
